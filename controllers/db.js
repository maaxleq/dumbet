const mysql = require("mysql");
const readlineSync = require("readline-sync");

const connection = mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS || readlineSync.question("Database password : "),
    database: process.env.DB_NAME,
});

connection.connect(err => {
    if (err){
        throw err;
    }
    else {
        console.log("Connected to mySQL database");
    }
});

const helper = {};

helper.query = (queryString, callback) => {
    db.query(queryString, callback);
}

module.exports = helper;