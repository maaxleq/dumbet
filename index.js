global.dirs = {};
global.dirs["root"] == __dirname;

require("dotenv").config();

const express = require("express");
const http = require("http");
const path = require("path");

const app = express();

app.use(express.static(path.join(__dirname, "assets")));

http.createServer(app).listen(process.env.PORT, process.env.HOST);
